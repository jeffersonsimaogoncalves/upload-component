<?php
 
/**
 * Componente do cake para upload de arquivos do tipo imagem.  
 *
 * @exemple
 * 
 *       // Configurando upload da imagem
 *       $config = [
 *           'file' => $data['Entidade']['file'],
 *           'directory' => 'brasoes/'
 *       ];
 *
 *       // Limitando tipos de upload
 *       $upload = $this->Upload->setType(['gif', 'jpeg', 'png', 'jpg'])
 *                       ->setCustomName('nomeArquivoPersonalizado')->inicialize($config);
 * 
 *      if ($upload == true) {
 *         echo $this->Upload->getUploadFileName();
 *      }
 *
 *
 * @author Tayron Miranda <dev@tayron.com.br>
 * @since 21/03/2012
 */
class UploadComponent extends Component {
 
    /**
     * Define o tipo de arquivo permitido no upload.
     * 
     * @access private
     * @var array Exemplo e op��es: array( 'zip', 'jpg', 'rar' ).
     */
    private $type = array();
 
    /**
     * Armazena o nome que o arquivo dever� receber.
     * 
     * @access private
     * @var string Exemplo: foto_fulado.
     */
    private $newName = null;
 
    /**
     * Armazena o nome personalizado para a imagem.
     * 
     * @access private
     * @var string Exemplo: foto_fulado_da_silva.
     */
    private $customName = null;
 
    /**
     * Armazena o nome do arquivo movido.
     * 
     * @access private
     * @var string Nome do arquivo
     */
    private $uploadFileName = null;
 
    /**
     * Tamanho maximo que o arquivo pode ter em kbytes.
     * Valor padr�o 10024 kbytes.
     * 
     * @access private
     * @var int Valor em kbytes, exemplo: 10024.
     */
    private $maxSize = 10024;
 
    /**
     * Armazena o nome do diretorio onde devera ser feito upload.
     * 
     * @access private
     * @var string Exemplo: banner.
     */
    private $uploadDirectory = null;
 
    /**
     * Armazena request data file.
     * 
     * @access private
     * @var array Request data file
     */
    private $fileData = array();
 
    /**
     * M�todo que permite recuperar informacao do arquivo.
     * 
     * @access public
     * @return array File data.
     */
    public function getFileData() {
        return $this->fileData;
    }
 
    /**
     * M�todo que permite recuperar nome do arquivo upado.
     * 
     * @access public
     * @return string Nome do arquivo upado.
     */
    public function getUploadFileName() {
        return $this->uploadFileName;
    }
 
    /**
     * M�todo que pega mensagem de erro.
     * 
     * @access public
     * @return string Mensagem de erro.
     */
    public function getMessage() {
        return $this->fileData['error'];
    }
 
    /**
     * M�todo que seta os tipos de arquivos permitidos.
     *      
     * @param array Array de tipos de arquivo.
     * @access public
     * @return object Retorna a pr�pria instancia da classe.
     */
    public function setType($type) {
        if (is_array($type)) {
            $this->type = $type;
        }
        return $this;
    }
 
    /**
     * M�todo que seta nome personalizado para a imagem.
     * 
     * @param String Nome personalizado para a imagem.
     * @access public
     * @return object Retorna a pr�pria instancia da classe.
     */
    public function setCustomName($name = null) {
        $this->customName = $name;
        return $this;
    }
 
    /**
     * M�todo que seta o diret�rio para upload, caso ele n�o exista, ser� criado 
     * um diret�rio com o nome informado.
     * 
     * @param string Nome do diretorio para upload.
     * @access public
     * @return boolean Retorna true caso consiga criar ou o diret�rio j� exista 
     * no servidor.
     */
    public function setUploadDirectory($directory) {
        $directory = '../webroot/' . $directory;
 
        if (is_dir($directory)) {
            $this->uploadDirectory = $directory;
            @chmod($directory, 0777);
        } else {
            if (mkdir($directory, 0777)) {
                $this->uploadDirectory = $directory;
            } else {
                $this->fileData['error'] = 'Erro ao criar o diretorio';
                return false;
            }
        }
 
        return true;
    }
 
    /**
     * M�todo que inicia e gerencia o upload do arquivo.
     *      
     * @param array $data Request data file.
     * @access public
     * @return boolean Retorna true  em caso de sucesso ou false em caso de falha.
     */
    public function inicialize($data) {
        // Setando file data 
        $this->fileData = $data['file'];
        $this->maxSize = $data['file']['size'];
 
        // Criando diretorio para upload caso nao tenha criado
        $directory = ( isset($data['directory']) ) ? $data['directory'] : 'upload';
        $this->setUploadDirectory($directory);
 
        // Se o arquivo for uma imagem fa�o validacoes e upload
        if ($this->isImage() AND $this->sizeIsAllowed() AND $this->formatIsAllowed()) {
            return $this->processUpload();
        }
 
        return false;
    }
 
    /**
     * M�todo que gerencia o processo de upload de arquivo.
     * 
     * @access private
     * @return boolean Retorna true em caso de sucesso.
     */
    private function processUpload() {
        if ($this->type != null AND $this->sizeIsAllowed()) {
            if ($this->formatIsAllowed())
                return $this->upload();
        }else {
            if ($this->sizeIsAllowed())
                return $this->upload();
        }
        return false;
    }
 
    /**
     * M�todo que verifica data type do arquivo para saber se ele � uma imagem.
     * 
     * @access private
     * @return boolean Retorna true caso o arquivo seja uma imagem.
     */
    private function isImage() {
        $typo = explode('/', $this->fileData['type']);
 
        if ($typo[0] != 'image') {
            $this->fileData['error'] = 'O arquivo n�o � uma imagem';
            return false;
        }
 
        return true;
    }
 
    /**
     * M�todo que pega o formato da imagem.
     * 
     * @access private
     * @return string Formato da imagem.
     */
    private function getFileType() {
        $type = explode('.', $this->_image);
        return trim(strtolower($type[1]));
    }
 
    /**
     * M�todo que verifica se o formato do arquivo est� permitido para upload.
     * 
     * @access private
     * @return boolean Retorna true caso o arquivo seja permitido.
     */
    private function formatIsAllowed() {
        $data = str_replace('image/', '', $this->fileData['type']);
 
        if ($this->type != null) { 
            if (array_search($data, $this->type) === false) {
                $this->fileData['error'] = 'O formato do arquivo nao � permitido para upload';
                return false;
            }
        }
 
        return true;
    }
 
    /**
     * M�todo que verifica se o tamanho do arquivo est� permitido para upload.
     * 
     * @access private
     * @return boolean Retorna true caso o arquivo seja permitido.
     */
    private function sizeIsAllowed() {
        if ($this->fileData['size'] > $this->maxSize) {
            $this->fileData['error'] = 'O tamanho do arquivo � superior ao permitido';
            return false;
        }
 
        return true;
    }
 
    /**
     * M�todo que verifica se o diretorio onde vai ser feito upload existe e 
     * verifica se n�o h� um arquivo com o mesmo nome, caso haja altera o nome.
     * 
     * @access private
     * @return boolean Retorna true caso consiga fazer o upload da imagem.
     */
    private function upload() {
        if ($this->uploadDirectory != null) {
 
            if (file_exists($this->getNameFile())) {
                $name = ( $this->isImage() ) ? 'imagem_' : 'arquivo_';
                $this->newName = $name . time() . date('dmYhis');
            }
 
            return $this->move();
        }
 
        $this->fileData['error'] = 'N�o foi informado diretorio para upload';
        return false;
    }
 
    /**
     * M�todo que move o arquivo do diret�rio temporario para para o diret�rio 
     * do sistema informado.
     * 
     * @access private
     * @return boolean Retorna true em caso de sucesso.
     */
    private function move() {
        // Se o arquivo ja nao existir no servidor copio ele pro diretorio especificado
        if (move_uploaded_file($this->fileData['tmp_name'], $this->getNameFile())) {
            $this->fileData['error'] = 'Arquivo upado com sucesso';
            return true;
        }
 
        $this->fileData['error'] = 'Nao foi possivel mover o arquivo';
        return false;
    }
 
    /**
     * M�todo que retira acentos do nome dos arquivos.
     *
     * @access private
     * @return string Nome do arquivo sem acentua��o.
     */
    private function removeAcentWord($string = null) {
        if ($string != null) {
            $array1 = array('�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
                '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�');
 
            $array2 = array('a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o',
                'o', 'o', 'o', 'u', 'u', 'u', 'u', 'c', 'A', 'A', 'A', 'A', 'A', 'E', 'E', 'E', 'E', 'I',
                'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'C');
 
            return str_replace($array1, $array2, $string);
        } else { 
            $this->fileData['error'] = 'Deve-se informar um texto, palavra para poder retirar os acentos';
            return $string;
        }
    }
 
    /**
     * M�todo que retorna o nome do diret�rio + nome do arquivo updado.
     * 
     * @access private
     * @return string Nome diret�rio + nome do arquivo updado.
     */
    private function getNameFile() {
        $type = explode('.', $this->fileData['name']);
 
        $name = (!is_null($this->newName)) 
            ? $this->removeAcentWord(strtolower($this->newName)) . '.' . $type[1] 
            : time() . $this->removeAcentWord(strtolower($this->fileData['name']));
        
        // Verificando se foi passado algum nome personalizado para imagem				 
        $this->uploadFileName = ( $this->customName != null ) 
            ? $this->removeAcentWord($this->customName) . '.' . $type[1] : $name;
 
        return $this->uploadDirectory . $this->uploadFileName;
    }
 
}