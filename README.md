# CakePHP - Upload Component#

Componente para CakePHP 2.x para upload e imagem.


**Tutorial:**
Para usar o component, carregue-o em seu controle, exemplo: ClienteController.

```
#!php
...
class ClienteController extends AppController{
   $components = ['Upload'];
...
```


Agora em sua action para usar o componente basta seguir o exemplo abaixo:


```
#!php

 *       // Configurando upload da imagem
 *       $config = [
 *           'file' => $data['Client']['file'],
 *           'directory' => 'clients/'
 *       ];
 *
 *       // Limitando tipos de upload
 *       $upload = $this->Upload->setType(['gif', 'jpeg', 'png', 'jpg'])
 *                       ->setCustomName('nomeArquivoPersonalizado')->inicialize($config);
 * 
 *      if ($upload == true) {
 *         echo $this->Upload->getUploadFileName(); // retorna o nome da imagem gravada no servidor
 *      }
```